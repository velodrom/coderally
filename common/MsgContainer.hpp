#pragma once
#include <stdint.h>
#include "IOStructs.hpp"
#include <cereal/archives/portable_binary.hpp>

enum MsgIds
{
    CarControlMsg = 0,
    SensorDataMsg,
    UndefinedMsg = 0xF
};

struct MsgHeader
{
public:
    template <class Archive>
    void serialize( Archive & ar )
    {
        ar( msgId );
    }

    uint32_t msgId;
};

union MsgPayload
{
    CarControlData carCtrlData;
    SensorData     sensorData;

    template <class Archive>
    void serialize( Archive & ar )
    {
        ar( sensorData );
    }
};

class MsgContainer
{
public:
    MsgContainer();
    template<typename PayloadType>
    MsgContainer(const PayloadType payloadIn);

    template <class Archive>
    void serialize( Archive & ar )
    {
        ar( header, payload );
    }

    MsgHeader  header;
    MsgPayload payload;
};


