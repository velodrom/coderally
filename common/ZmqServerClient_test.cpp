#include <gtest/gtest.h>
#include <thread>
#include "ZmqServer.hpp"
#include "ZmqClient.hpp"

TEST(ZmqServerClientTest, ConnectServerAndClientSucceeds)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, ClientSendsSimpleValue)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);
    std::string sendMsg = "11";
    cli1.Send(sendMsg);
    std::string recMsg = srv1.Recv();
    EXPECT_EQ(sendMsg.size(), recMsg.size());
    EXPECT_EQ(sendMsg, recMsg);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, ServerRespondsSimpleValue)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);

    std::string sendMsg = "11";
    cli1.Send(sendMsg);
    std::string recMsg = srv1.Recv();
    srv1.Send(recMsg);
    
    std::string cliRecMsg = cli1.Recv();
    EXPECT_EQ(sendMsg.size(), cliRecMsg.size());
    EXPECT_EQ(sendMsg, cliRecMsg);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, TwoMessagesReceivedInOrder)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);

    std::string sendMsg1 = "11";
    std::string sendMsg2 = "12";
    cli1.Send(sendMsg1);
    cli1.Send(sendMsg2);
    std::string recMsg1 = srv1.Recv();
    std::string recMsg2 = srv1.Recv();
    
    EXPECT_EQ(sendMsg1.size(), recMsg1.size());
    EXPECT_EQ(sendMsg1, recMsg1);
    EXPECT_EQ(sendMsg2.size(), recMsg2.size());
    EXPECT_EQ(sendMsg2, recMsg2);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, ClientAndServerSendSimultaneously)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);

    std::string sendMsg1 = "MsgFromClient";
    std::string sendMsg2 = "MsgFromServer";
    cli1.Send(sendMsg1);
    srv1.Send(sendMsg2);
    std::string recMsg1 = srv1.Recv();
    std::string recMsg2 = cli1.Recv();
    
    EXPECT_EQ(sendMsg1.size(), recMsg1.size());
    EXPECT_EQ(sendMsg1, recMsg1);
    EXPECT_EQ(sendMsg2.size(), recMsg2.size());
    EXPECT_EQ(sendMsg2, recMsg2);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, MessagesSentBeforeClientConnectsAreDiscarded)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);

    std::string sendMsg1 = "ShouldNotBeDelivered1";
    std::string sendMsg2 = "ShouldNotBeDelivered2";
    srv1.Send(sendMsg1);
    srv1.Send(sendMsg2);

    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);
    std::string sendMsg3 = "ShouldBeDelivered1";
    srv1.Send(sendMsg3);
    std::string recMsg1 = cli1.Recv();
    
    EXPECT_EQ(sendMsg3.size(), recMsg1.size());
    EXPECT_EQ(sendMsg3, recMsg1);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, MsgContainerIsReceivedCorrectly)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);

    CarControlData ctrlDataIn{.steer = 11, .throttle = 12};
    MsgContainer containerOut(ctrlDataIn);

    std::string sendMsg = ZmqService::SerializeMsg(containerOut);
    cli1.Send(sendMsg);
    std::string recMsg = srv1.Recv();
    
    MsgContainer containerIn = ZmqService::DeserializeMsg(recMsg);

    EXPECT_EQ(ctrlDataIn.steer,    containerIn.payload.carCtrlData.steer);
    EXPECT_EQ(ctrlDataIn.throttle, containerIn.payload.carCtrlData.throttle);
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, MsgContainerSensorDataIsReceivedCorrectly)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);

    SensorData sensorData{.photoResistors = {1,2,3,4,5,6,7,8}};
    MsgContainer containerOut(sensorData);

    std::string sendMsg = ZmqService::SerializeMsg(containerOut);
    cli1.Send(sendMsg);
    std::string recMsg = srv1.Recv();
    
    MsgContainer containerIn = ZmqService::DeserializeMsg(recMsg);

    for (int i = 0; i < numPhotoResistors; i++)
    {
        EXPECT_EQ(sensorData.photoResistors[i], containerIn.payload.sensorData.photoResistors[i]);
    }
    cli1.Disconnect();
}

TEST(ZmqServerClientTest, ServerWaitsForClientConnection)
{
    const std::string address = "tcp://127.0.0.1:5555";
    ZmqServer srv1(address, ZMQ_PAIR);

    std::thread first(&ZmqServer::WaitClientConnection, &srv1, 3000);
    sleep(1);
    ZmqClient cli1;
    cli1.Connect(address, ZMQ_PAIR);
    cli1.Disconnect();
    // Todo: Add persistent monitoring. Current monitoring catches one event
    first.join();
}