#pragma once
#include <vector>
#include <thread>
#include <functional>
#include "zmq.hpp"
#include "ZmqMsgIf.hpp"
#include "ZmqService.hpp"

class ConnectionMonitor : public zmq::monitor_t {
public:
    ConnectionMonitor() : 
        handle_accepted([this] () { this->do_nothing();}),
        handle_closed([this] () { this->do_nothing();})
    {
    }

    void do_nothing()
    {
    }

    void add_handle_accepted(std::function<void(void)> handle_accepted_in)
    {
        handle_accepted = std::move(handle_accepted_in);
    }

    void add_handle_closed(std::function<void(void)> handle_closed_in)
    {
        handle_closed = std::move(handle_closed_in);
    }

    void on_event_accepted(const zmq_event_t& event,
                            const char* addr) override
    {
        printf("%d: ZmqServer: Inbound connection accepted at: %s\n", getpid(), addr);
        handle_accepted();
    }

    void on_event_closed(const zmq_event_t& event,
                        const char* addr) override
    {
        printf("%d: ZmqServer: Inbound connection closed at: %s\n", getpid(), addr);
        handle_closed();
    }

    std::function<void(void)> handle_accepted;
    std::function<void(void)> handle_closed;
};

class ZmqServer : public ZmqMsgIf
{
public:
    ZmqServer(const std::string address, int socketType);
    ~ZmqServer();
    void BindSocket(zmq::socket_t *socket, const std::string address);
    void MonitorSocket(const int timeoutMs, const int events = ZMQ_EVENT_ACCEPTED | ZMQ_EVENT_CLOSED);
    void WaitClientConnection(const int timeoutMs = 1000);
    void ClientConnected();
    void ClientDisconnected();
   
private:
    ZmqServer();
    std::thread* monitorThread;
};