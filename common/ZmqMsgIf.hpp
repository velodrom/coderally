#pragma once
#include <string>
#include <unistd.h>
#include <exception>
#include "zmq.hpp"

enum ConnStatus
{
    Disconnected,
    Connected
};

struct ZmqSendException : public std::exception {
   const char * what () const throw () {
      return "Failed to send message!";
   }
};

struct ZmqRecvException : public std::exception {
   const char * what () const throw () {
      return "Failed to receive message!";
   }
};

const int zmqDefaultTimeout = 100;

class ZmqMsgIf
{
public:
    zmq::context_t* CreateContext();
    zmq::socket_t*  CreateSocket(zmq::context_t *context, int socketType);
    void            Send(const std::string &serializedMsg, zmq::send_flags flags = zmq::send_flags::none, int timeoutMs = zmqDefaultTimeout);
    std::string     Recv(int timeoutMs = zmqDefaultTimeout);

protected:
    std::string          socketAddress;
    int                  socketType;
    zmq::context_t*      context;
    zmq::socket_t*       socket;
    ConnStatus           connStatus;

    ZmqMsgIf();
};