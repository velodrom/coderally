#include <errno.h>
#include "ZmqMsgIf.hpp"

ZmqMsgIf::ZmqMsgIf() : 
    connStatus(Disconnected)
{
}

zmq::context_t* ZmqMsgIf::CreateContext()
{
    uint32_t        numThreads = 2;
    zmq::context_t *zmqContext;
    try
    {
        zmqContext = new zmq::context_t(numThreads);
        return zmqContext;
    }
    catch (const std::exception& e)
    {
        printf("%d: Error: %s\n", getpid(), e.what());
    }
    return NULL;
}

zmq::socket_t* ZmqMsgIf::CreateSocket(zmq::context_t *context, int socketType)
{
    zmq::socket_t *zmqSocket;
    try
    {
        zmqSocket = new zmq::socket_t(*context, socketType);
        zmqSocket->set(zmq::sockopt::immediate, false); // Queue messages only to established connections
        // zmqSocket->set(zmq::sockopt::linger, 0); // Discard pending messages immediately after disconnect or close
        zmqSocket->set(zmq::sockopt::rcvtimeo, zmqDefaultTimeout); // Receive timeout (ms)
        zmqSocket->set(zmq::sockopt::sndtimeo, zmqDefaultTimeout); // Send timeout (ms)
        return zmqSocket;
    }
    catch (const std::exception& e)
    {
        printf("%d: Error: %s\n", getpid(), e.what());
    }
    return NULL;
}

void ZmqMsgIf::Send(const std::string &serializedMsg, zmq::send_flags flags, int timeoutMs)
{
    if(socket != nullptr)
    {
        socket->set(zmq::sockopt::sndtimeo, timeoutMs);
        zmq::message_t msg(serializedMsg);
        // Don't catch exception here
        zmq::send_result_t sendRes = socket->send(msg, zmq::send_flags::none);
        if (sendRes.has_value())
        {
            printf("%d: Sent %d bytes\n", getpid(), sendRes);
        }
        else if (errno > 0)
        {
            char errnoBuf[256];
            char* errPtr = strerror_r(errno, &errnoBuf[0], sizeof(errnoBuf));
            printf("%d: Send failed, errno[%d]: %s\n", getpid(), errno, errPtr);
        }
    }
    else
    {
        printf("%d: socket is uninitialized\n", getpid());
    }
}

std::string ZmqMsgIf::Recv(int timeoutMs)
{
    zmq::message_t buf;
    if (socket != nullptr)
    {
        socket->set(zmq::sockopt::rcvtimeo, timeoutMs); // Receive timeout (ms)
        // Don't catch exception here
        zmq::recv_result_t recBufSize = socket->recv(buf, zmq::recv_flags::none);
        uint64_t* data = buf.data<uint64_t>();
        if (recBufSize.has_value())
        {
            printf("%d: Received recBufSize.value(): %zu buf.size(): %zu: data: %s\n", 
                getpid(), 
                recBufSize.value(),
                buf.size(),
                buf.str().c_str());
        }
        else if (errno > 0)
        {
            char errnoBuf[256];
            char* errPtr = strerror_r(errno, &errnoBuf[0], sizeof(errnoBuf));
            printf("%d: Receive failed, errno[%d]: %s\n", getpid(), errno, errPtr);
            throw ZmqRecvException();
        }
    }
    else
    {
        printf("%d: socket is uninitialized\n", getpid());
    }

    return buf.to_string();
}