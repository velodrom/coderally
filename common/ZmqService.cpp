#include "ZmqService.hpp"

namespace ZmqService
{
    std::string SerializeMsg(const MsgContainer &msg)
    {
        std::stringstream ss;
        {
            cereal::PortableBinaryOutputArchive outArch(ss);
            outArch(msg);
        }
        return ss.str();
    }

    MsgContainer DeserializeMsg(const std::string &str)
    {
        std::stringstream ss(str);
        MsgContainer msgContainer;
        {
            cereal::PortableBinaryInputArchive inArch(ss);
            inArch(msgContainer);
        }
        return msgContainer;
    }
}