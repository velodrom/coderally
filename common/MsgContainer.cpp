#include "MsgContainer.hpp"

MsgContainer::MsgContainer() :
    header({.msgId = UndefinedMsg})
{
}

template<>
MsgContainer::MsgContainer<CarControlData>(const CarControlData payloadIn)
{
    header.msgId = CarControlMsg;
    payload.carCtrlData = payloadIn;
}

template<>
MsgContainer::MsgContainer<SensorData>(const SensorData payloadIn)
{
    header.msgId = SensorDataMsg;
    payload.sensorData = payloadIn;
}