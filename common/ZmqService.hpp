#pragma once
#include <string>
#include "zmq.hpp"
#include "MsgContainer.hpp"

namespace ZmqService
{
    std::string     SerializeMsg(const MsgContainer &msg);
    MsgContainer    DeserializeMsg(const std::string &str);
}