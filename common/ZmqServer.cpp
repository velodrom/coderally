#include <ZmqServer.hpp>

ZmqServer::ZmqServer(const std::string address, int socketType) :
    monitorThread(nullptr)
{
    connStatus    = Disconnected;
    socketAddress = address;
    socketType    = socketType;
    context       = CreateContext();
    socket        = CreateSocket(context, socketType);
    BindSocket(socket, socketAddress);
}

ZmqServer::~ZmqServer()
{
    socket->unbind(socketAddress);
    delete socket;
    delete context;
}

void ZmqServer::BindSocket(zmq::socket_t *socket, const std::string address)
{
    try
    {
        socket->bind(address);
        printf("%d: Bound %s\n", getpid(), address.c_str());
    }
    catch (const std::exception& e)
    {
        printf("%d: Error: %s\n", getpid(), e.what());
    }
}

void ZmqServer::ClientConnected()
{
    connStatus = Connected;
}

void ZmqServer::ClientDisconnected()
{
    connStatus = Disconnected;
}

void ZmqServer::MonitorSocket(const int timeoutMs, const int events)
{
    ConnectionMonitor conMon;
    if (events & ZMQ_EVENT_ACCEPTED)
        conMon.add_handle_accepted([this](){this->ClientConnected();});
    if (events & ZMQ_EVENT_CLOSED)
        conMon.add_handle_closed([this](){this->ClientDisconnected();});
    conMon.init(*socket, "inproc://conmon", events);
    conMon.check_event(timeoutMs);
}

void ZmqServer::WaitClientConnection(const int timeoutMs)
{
    const int events = ZMQ_EVENT_ACCEPTED;
    MonitorSocket(timeoutMs, events);
}
