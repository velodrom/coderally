#include <unistd.h>
#include "ZmqClient.hpp"

ZmqClient::ZmqClient()
{
    connStatus    = Disconnected;
    socketAddress = "";
    socketType    = -1;
    context       = nullptr;
    socket        = nullptr;
}

ZmqClient::~ZmqClient()
{
    Disconnect();
    delete socket;
    delete context;
}

bool ZmqClient::Connect(const std::string &address, const int &socketTypeArg)
{
    if (connStatus == Disconnected)
    {
        try
        {
            context = CreateContext();
            socket  = CreateSocket(context, socketTypeArg);
            socket->connect(address);
            usleep(5000); // Wait for connection to get established
            connStatus = Connected;
            socketAddress = address;
            socketType = socketTypeArg;
            printf("%d: Client connected to %s!\n", getpid(), static_cast<std::string>(address).c_str());
        }
        catch(const std::exception& e)
        {
            printf("%s\n", e.what());
            return false;
        }
    }
    else
    {
        printf("%d: Client already connected! Disconnect before connecting again.\n", getpid());
        return false;
    }
    return true;
}

void ZmqClient::Disconnect()
{
    if (connStatus == Connected)
    {
        socket->disconnect(socketAddress);
        socketAddress = "";
        socketType = -1;
        connStatus = Disconnected;
    }
}
