#include <gtest/gtest.h>
#include "MsgContainer.hpp"
#include "IOStructs.hpp"

TEST(MsgContainerTest, CreateCarControlMsg)
{
    CarControlData carCtrl = {.steer = 11, .throttle = 12};
    MsgContainer newMsg(carCtrl);
    ASSERT_EQ(CarControlMsg,    newMsg.header.msgId);
    ASSERT_EQ(carCtrl.steer,    newMsg.payload.carCtrlData.steer);
    ASSERT_EQ(carCtrl.throttle, newMsg.payload.carCtrlData.throttle);
}

TEST(MsgContainerTest, CreateSensorDataMsg)
{
    SensorData sensorData = {.photoResistors = {0,1,2,3,4,5,6,7}};
    MsgContainer newMsg(sensorData);
    ASSERT_EQ(SensorDataMsg, newMsg.header.msgId);
    for (uint32_t i = 0; i < numPhotoResistors; i++)
    {
        ASSERT_EQ(sensorData.photoResistors[i], newMsg.payload.sensorData.photoResistors[i]);
    }
}