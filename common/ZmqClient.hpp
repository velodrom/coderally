#pragma once
#include "zmq.hpp"
#include "ZmqMsgIf.hpp"
#include "ZmqService.hpp"



class ZmqClient : public ZmqMsgIf
{
public:
    ZmqClient();
    ~ZmqClient();
    bool Connect(const std::string &address, const int &socketType);
    void Disconnect();

private:
};
