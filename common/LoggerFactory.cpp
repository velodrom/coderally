#include "LoggerFactory.hpp"
#include <stdio.h>

Logger LoggerFactory::GetLogger()
{
    //CreateLogDir();
    static std::shared_ptr<spd::logger> logInstance = spd::basic_logger_mt("CodeRally", "logs/race.log"); // Instantiated on first call only
    return logInstance;
}

LoggerFactory::LoggerFactory()
{
    printf("Creating logger\n");
}

void LoggerFactory::CreateLogDir()
{
    const int dir = system("mkdir -p logs");
    if (dir != 0)
    {
        printf("Created directory logs\n");
    }
}