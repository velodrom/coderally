#include <gtest/gtest.h>
#include <LoggerFactory.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <regex>

using namespace ::testing;

class LoggerFactoryTest : public Test
{
public:
    LoggerFactoryTest() : logger(LoggerFactory::GetLogger())
    {
    }

    ~LoggerFactoryTest()
    {
    }
protected:
    std::shared_ptr<spd::logger> logger;
};

TEST_F(LoggerFactoryTest, SimpleLogWrite)
{
    logger->info("Test123");
    std::string   logLine;
    std::ifstream logFile;
    logFile.open("race.log");
    if (logFile.is_open())
    {
        getline(logFile, logLine);
        logFile.close();
    }

    std::regex expr(".*Test123");
    ASSERT_TRUE(std::regex_match(logLine, expr));
}

TEST_F(LoggerFactoryTest, TwoInstancesPointToSameObject)
{
    std::shared_ptr<spd::logger> logger1 = LoggerFactory::GetLogger();
    std::shared_ptr<spd::logger> logger2 = LoggerFactory::GetLogger();

    ASSERT_TRUE(logger1 == logger2);
}