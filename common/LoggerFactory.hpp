#ifndef LOGGERFACTORY_HPP
#define LOGGERFACTORY_HPP
#include "spdlog/spdlog.h"

namespace spd = spdlog;
typedef std::shared_ptr<spd::logger> Logger;

class LoggerFactory
{
public:
    static Logger GetLogger();
private:
    static void CreateLogDir();
    LoggerFactory();
    LoggerFactory(LoggerFactory const&);           // Copy constructor declared private for singleton
    void operator=(LoggerFactory const &);  // Copy assignment declared private for singleton
};

#endif