#ifndef KILLSWITCH_FACTORY_HPP
#define KILLSWITCH_FACTORY_HPP
#include "KillswitchIf.hpp"
#include "XBeeKillswitch.hpp"

typedef enum KillswitchType
{
    ALWAYS_ON       = 0,
    XBEE_KILLSWITCH = 1,
    UNDEFINED
} KillswitchType;

class KillswitchFactory
{
public:
    static KillswitchFactory GetFactory();
    static KillswitchIf*     CreateKillswitch(KillswitchType type);
private:
    KillswitchFactory();
};

#endif //KILLSWITCH_FACTORY_HPP