#ifndef XBEE_KILL_SWTICH_HPP
#define XBEE_KILL_SWTICH_HPP
#include "HWConfig.hpp"
#include "KillswitchIf.hpp"
#include "LoggerFactory.hpp"
#include "Timer.hpp"
#include <xbeep.h>

typedef struct xbee     XBee;
typedef struct xbee_con XCon;
// Compiler can pad the structure as it wishes unless the packed attribute is specified
// This has a huge impact when using reinterpret_cast
// packed            | without packed
// 0x00: numSamples  | 0x00: numSamples
// 0x01: dioChanMask | 0x02: dioChanMask
// 0x03: aioChanMask | 0x04: aioChanMask
// 0x05: adioValue   | 0x06: adioValue
// 0x06: padding     | 0x08: padding
typedef struct __attribute__((packed)) xbeeApiFrameData
{
    uint8_t  numSamples;
    uint16_t dioChanMask; // Digital IO channel mask
    uint16_t aioChanMask; // Analog IO channel mask
    uint8_t  adioValue;   // Analog or digital value
    uint16_t padding;
} xbeeApiFrameData;

typedef struct lastSwitchState
{
    bool   isSwitchOn;
    Timer  lastUpdateTime;

    lastSwitchState() :
        isSwitchOn(true),
        lastUpdateTime() {};
} lastSwitchState;

class XBeeKillswitch : public KillswitchIf
{
public:
    XBeeKillswitch();
    ~XBeeKillswitch();

    bool              SetupLink(uint64_t localXBeeAddr);
    void              UpdateKillswitch();
    xbeeApiFrameData* GetNewPacket();
    KillswitchStatus  GetKillswitchStatus();
private:
    bool             ConvertToSwitchStatus(uint16_t adioValue);

    XBee            *xbee;
    XCon            *xcon;
    lastSwitchState  switchStatus;
    uint32_t         numSwitchUpdates;
    Logger           logger;
};

#endif