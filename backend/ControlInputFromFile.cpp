#include "ControlInputFromFile.hpp"

ControlInputFromFile::ControlInputFromFile(string fileName)
{
    currentLine = 0;
    inputFile = fileName;
    filePtr = NULL;
}

ControlInputFromFile::~ControlInputFromFile()
{
    if (filePtr != NULL)
        fclose(filePtr);
}

ControlData ControlInputFromFile::ControlCar(Sensors const &sensorInput)
{
    ControlData outputData;
    if (currentLine == 0)
        OpenInputFile();

    uint32_t steer;
    uint32_t throttle;
    int numValuesRead = fscanf(filePtr, "str:%d;thr:%d\n", &steer, &throttle);
    if (numValuesRead != EOF)
    {
        outputData.steer    = steer;
        outputData.throttle = throttle;
        lastInput = outputData;
        currentLine++;
    }
    else
    {
        outputData = lastInput;
    }
    return outputData;
}

void ControlInputFromFile::OpenInputFile()
{
    filePtr = fopen(inputFile.c_str(), "r");
    if (filePtr == NULL)
    {
        printf("Failed to Open file: %s\n", inputFile.c_str());
        exit(-1);
    }
}
