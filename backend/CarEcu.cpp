#include "CarEcu.hpp"

CarEcu::CarEcu()
{
    pwmCtrl = new PwmController();
    pwmCtrl->InitGpioPin(motorEscGpioPin);
    pwmCtrl->InitGpioPin(steerServoGpioPin);
    uint32_t motorOffPwmWidth    = 1000;
    uint32_t steerCenterPwmWidth = 1500;
    pwmCtrl->SendPwmSignal(motorEscGpioPin, motorOffPwmWidth);
    pwmCtrl->SendPwmSignal(steerServoGpioPin, steerCenterPwmWidth);
}

void CarEcu::ControlCar(CarControlData &ctrlData)
{
    uint32_t motorPwmWidth = LimitMotorPwm(ctrlData.throttle);
    uint32_t steerPwmWidth = LimitServoPwm(ctrlData.steer);

    pwmCtrl->SendPwmSignal(motorEscGpioPin, motorPwmWidth);
    pwmCtrl->SendPwmSignal(steerServoGpioPin, steerPwmWidth);
}

uint32_t CarEcu::LimitMotorPwm(uint32_t motorPwm)
{
    if (motorPwm > maxMotorPwm)
        return maxMotorPwm;
    if (motorPwm < minMotorPwm)
        return minMotorPwm;
    return motorPwm;
}

uint32_t CarEcu::LimitServoPwm(uint32_t servoPwm)
{
    if (servoPwm > maxServoPwm)
        return maxServoPwm;
    if (servoPwm < minServoPwm)
        return minServoPwm;
    return servoPwm;

}
