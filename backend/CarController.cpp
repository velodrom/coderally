#include <unistd.h>
#include "CarController.hpp"
#include "ZmqService.hpp"

CarController::CarController(ZmqServer &zmqServerIn, KillswitchIf *killSwitchIn) :
    zmqServer(zmqServerIn),
    killSwitch(killSwitchIn)
{
    carEcu = new CarEcu();
}

void CarController::SendSensorDataToDriverAi(SensorData &sensorData)
{
    try
    {
        MsgContainer containerOut(sensorData);
        std::string sendMsg = ZmqService::SerializeMsg(containerOut);
        zmqServer.Send(sendMsg, zmq::send_flags::none, 5000);
    }
    catch(const std::exception &ex)
    {
        printf("%s:%d: Failed with exception \"%s\", error\n", __FILE__, __LINE__, ex.what());
    }
}

bool CarController::ReceiveControlDataFromAi(CarControlData &ctrlData)
{
    bool recOk = false;
    printf("%d: HW Waiting ctrl data from AI\n", getpid());
    std::string recMsg = zmqServer.Recv(5000);
    MsgContainer containerIn = ZmqService::DeserializeMsg(recMsg);
    if (containerIn.header.msgId == CarControlMsg)
    {
        printf("%d: HW Received ctrl data from AI: str:%d, thr:%d\n", getpid(), ctrlData.steer, ctrlData.throttle);
        ctrlData.throttle = containerIn.payload.carCtrlData.throttle;
        ctrlData.steer    = containerIn.payload.carCtrlData.steer;
        recOk = true;
    }
    else
    {
        printf("%d: Received unexpected message type from AI: %d\n", getpid(), containerIn.header.msgId);  
    }
    return recOk;
}

void CarController::DriveLoop()
{
    printf("%d: Running the car\n", getpid());
    try
    {
        while(carRunning)
        {
            if (killSwitch->GetKillswitchStatus() == SWITCH_OFF)
            {
                CarControlData ctrlData = {steer : 1500, throttle : minMotorPwm};
                SensorData sensorInput = sensorProvider->ReadCarSensors();
                SendSensorDataToDriverAi(sensorInput);
                ReceiveControlDataFromAi(ctrlData);

                carEcu->ControlCar(ctrlData);
                sleep(1);
            }
            else
            {
                printf("Killswitch activated! Stopping the car.\n");
                StopCar();
            }
        }
    }
    catch(const std::exception& e)
    {
        fflush(stdout);
        printf("%s:%d failed with exception \"%s\"\n", __FILE__, __LINE__, e.what());
        StopCar();
    }
    
    
}

bool CarController::StartCar(void)
{
    try
    {
        sensorProvider = new SensorDataProvider();
        if (killSwitch->GetKillswitchStatus() == SWITCH_OFF)
        {
            SendStartSignalToSpeedController();
            carRunning = true;
        }
    }
    catch(const std::exception& ex)
    {
        printf("%s:%d: Failed with exception \"%s\", error\n", __FILE__, __LINE__, ex.what());
        carRunning = false;
    }
    
    return carRunning;
}

void CarController::StopCar(void)
{
    carRunning = false;
    CarControlData stopCar = {steer : idleServoPwm, throttle : minMotorPwm};
    carEcu->ControlCar(stopCar);
    delete(sensorProvider);
}

void CarController::SendStartSignalToSpeedController(void)
{
    CarControlData startSignal = {steer : idleServoPwm, throttle : idleMotorPwm};
    printf("%d: Arming ESC...\n", getpid());
    carEcu->ControlCar(startSignal);
    sleep(3); // Wait for Electronic Speed Controller to synchronize with idle throttle signal
}
