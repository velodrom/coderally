#ifndef HW_CONFIG_HPP
#define HW_CONFIG_HPP
#include <stdint.h>

const uint64_t xbeeLocalAddress  = 0x0013A200409922E5L;
const uint8_t  motorEscGpioPin   = 27;
const uint8_t  steerServoGpioPin = 17;

#endif //HW_CONFIG_HPP