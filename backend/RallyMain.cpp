#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <dlfcn.h>
#include <unistd.h>
#include <string>
#include "AiController.hpp"
#include "CarController.hpp"
#include "DriverAi.hpp"
#include "KillswitchFactory.hpp"
#include "ZmqServer.hpp"
#include "ControlInputFromFile.hpp"

using namespace std;

string inputFileName    = "";
string zmqServerAddress = "tcp://127.0.0.1:6666";

template<class TYPE>
TYPE function_cast(void * symbol)
{
    assert(sizeof(void *) == sizeof(TYPE));
    union
    {
        void * symbol;
        TYPE function;
    } cast;
    cast.symbol = symbol;
    return cast.function;
}

void StartDriverAiProcess()
{
    pid_t pid = fork();

    if (pid != 0)
    {
        printf("%d: Starting AI process\n", getpid());
        AiController *aiCtrl = new AiController(ControllerType::UserAiLib);
        sleep(1);
        if (aiCtrl->ConnectToServer(zmqServerAddress, ZMQ_PAIR))
        {
            sleep(3);
            aiCtrl->MessageLoop();
        }
        printf("%d: Stopping AI process\n", getpid());
        exit(0);
    }
}

void ParseInputArgs(int argc, char* argv[])
{
    for (int idx = 0; idx < argc; idx++)
    {
        string argAtIdx = argv[idx];
        if (argAtIdx == "-d")
        {
            inputFileName = argv[++idx];
        }
    }
}

void StartCarControl()
{
    ZmqServer    *msgSrv     = new ZmqServer(zmqServerAddress, ZMQ_PAIR);
    KillswitchIf *killswitch = KillswitchFactory::GetFactory().CreateKillswitch(XBEE_KILLSWITCH);
    const int timeoutMs      = 100000;
    msgSrv->WaitClientConnection(timeoutMs);
    CarController *carCntrl  = new CarController(*msgSrv, killswitch);
    if (carCntrl->StartCar())
    {
        printf("%d: Car started - entering drive loop\n", getpid());
        carCntrl->DriveLoop();
    }
}

int main(int argc, char* argv[])
{
    printf("\n--- CodeRally ---\n\n");
    ParseInputArgs(argc, argv);

    StartDriverAiProcess();
    StartCarControl();

    printf("\n --- CodeRally server Finished ---\n\n");
    return 0;
}
