#ifndef SENSOR_DATA_PROVIDER_HPP
#define SENSOR_DATA_PROVIDER_HPP
#include "AdcIf.hpp"
#include "IOStructs.hpp"
#include "LoggerFactory.hpp"
#include <stdint.h>
#include <unistd.h>
#include <string>

using namespace std;

const int photoResitorArrayGpioPin = 11;

class SensorDataProvider
{
public:
    SensorDataProvider();
    ~SensorDataProvider();
    SensorData ReadCarSensors();
    void ReadPhotoResistors(uint32_t (&photoArray)[numPhotoResistors]);
    uint32_t CorrectMagValue(uint32_t lightMagnitude, uint32_t sensorIdx);
    string CreateLogString(uint32_t (&photoArray)[numPhotoResistors]);
private:
    void LogMeasurements(uint32_t (&photoArray)[numPhotoResistors]);

    AdcIf *adcDevice;
    Logger logger;
};

#endif
