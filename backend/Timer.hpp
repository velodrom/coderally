#ifndef TIMER_HPP
#define TIMER_HPP
#include <boost/chrono.hpp>

using namespace boost::chrono;

class Timer
{
public:
    Timer() :
        start(system_clock::now())
    {}

    void Reset()
    {
        start = system_clock::now(); 
    }

    double Elapsed() const
    {
        system_clock::time_point stop = system_clock::now();
        milliseconds ms = duration_cast<milliseconds>(stop - start);
        return ms.count();
    }

private:
    system_clock::time_point start;
};

#endif //TIMER_HPP