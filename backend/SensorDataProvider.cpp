#include <sstream>
#include <SensorDataProvider.hpp>
#include <Mcp3204.hpp>

// First order polynomial for calibrating photoresistor errors
// Polynomial coefficients are stored in the array in descending order
// y_k = a1*x_k+a0
// [a0, a1]
static const float calibrationPolynoms[numPhotoResistors][2] = {
    {  15.7498,  0.0034},
    {-107.6905,  0.0785},
    {  82.4024, -0.0632},
    {  -1.8961, -0.0045},
    {  15.3420, -0.0567},
    {  62.5433, -0.0143},
    {  25.3096, -0.0381},
    { -91.7606,  0.0949}
};

SensorDataProvider::SensorDataProvider() : logger(LoggerFactory::GetLogger())
{
    adcDevice = new Mcp3204();
    adcDevice->Configure();
    if (adcDevice->IsConfigured() == false)
    {
        logger->error("SensorDataProvider::SensorDataProvider, adcDevice not configured");
    }
}

SensorDataProvider::~SensorDataProvider()
{
    delete adcDevice;
}

SensorData SensorDataProvider::ReadCarSensors()
{
    SensorData sensors;
    ReadPhotoResistors(sensors.photoResistors);

    return sensors;
}

void SensorDataProvider::ReadPhotoResistors(uint32_t (&photoArray)[numPhotoResistors])
{
    for (uint32_t sensorIdx = 0; sensorIdx < numPhotoResistors; sensorIdx++)
    {
        uint32_t lightMagnitude = adcDevice->Measure(sensorIdx);
        photoArray[sensorIdx]   = CorrectMagValue(lightMagnitude, sensorIdx);
    }
    LogMeasurements(photoArray);
}

uint32_t SensorDataProvider::CorrectMagValue(uint32_t lightMagnitude, uint32_t sensorIdx)
{
    float a0 = calibrationPolynoms[sensorIdx][0];
    float a1 = calibrationPolynoms[sensorIdx][1];
    int32_t  correction         = (int32_t)(a1*lightMagnitude + a0);
    int32_t calibratedMagnitude = (int32_t)(lightMagnitude + correction);
    return (calibratedMagnitude > 0 ? calibratedMagnitude : 0);
}

string SensorDataProvider::CreateLogString(uint32_t (&photoArray)[numPhotoResistors])
{
    std::stringstream logMsg;
    logMsg << "PhotoArray:";
    for (uint32_t sensorIdx = 0; sensorIdx < numPhotoResistors; ++sensorIdx)
    {
        logMsg << " " << std::to_string(sensorIdx) << ": " << std::to_string(photoArray[sensorIdx]);
    }
    return logMsg.str();
}

void SensorDataProvider::LogMeasurements(uint32_t (&photoArray)[numPhotoResistors])
{
    string logMsg = CreateLogString(photoArray);
    //printf("Meas: %s\n", logMsg.c_str());
    logger->info(logMsg);
}
