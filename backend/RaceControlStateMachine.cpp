#include "RaceControlStateMachine.hpp"

RaceControlStateMachine::RaceControlStateMachine(CarControlStateMachineIf *carStm) :
    StateMachine(ST_MAX_STATES),
    carStm(carStm)
{
    KillswitchFactory ksFactory = KillswitchFactory::GetFactory();
    killswitch = ksFactory.CreateKillswitch(XBEE_KILLSWITCH);
}

void RaceControlStateMachine::StartRace()
{
    uint32_t numRaceTicks = 2;

    for (uint32_t tickCount = 0; tickCount < numRaceTicks; tickCount++)
    {
        KillswitchStatus KillswitchStatus = killswitch->GetKillswitchStatus();
        if (KillswitchStatus == SWITCH_OFF)
        {
            printf("Killswitch is off. Running\n");
            this->Run();
        }
        else
        {
            printf("Killswitch is on. Stopping\n");
            this->Stop();
        }
    }
}

void RaceControlStateMachine::Stop()
{
    static const BYTE TRANSITIONS[] = 
    {                       // Current state
        EVENT_IGNORED,      // ST_STOPPED,
        ST_STOPPED,         // ST_READY,
        ST_STOPPED,         // ST_RUNNING,
    };
    ExternalEvent(TRANSITIONS[GetCurrentState()], NULL); 
    C_ASSERT((sizeof(TRANSITIONS)/sizeof(BYTE)) == ST_MAX_STATES);
}

void RaceControlStateMachine::Run()
{
    static const BYTE TRANSITIONS[] = 
    {                       // Current state
        ST_READY,           // ST_STOPPED,
        ST_RUNNING,         // ST_READY,
        ST_RUNNING,         // ST_RUNNING,
    };
    ExternalEvent(TRANSITIONS[GetCurrentState()], NULL); 
    C_ASSERT((sizeof(TRANSITIONS)/sizeof(BYTE)) == ST_MAX_STATES);
}

void RaceControlStateMachine::RcsmStop(const NoEventData*)
{
    printf("RaceControlStateMachine RcsmStop\n");
}

BOOL RaceControlStateMachine::RcsmInit(const NoEventData*)
{
    printf("RaceControlStateMachine RcsmInit\n");
    carStm->InitCarStm();
    return 1;
}

void RaceControlStateMachine::RcsmReady(const NoEventData*)
{
    printf("RaceControlStateMachine RcsmReady\n");
}

void RaceControlStateMachine::RcsmRun(const NoEventData*)
{
    printf("RaceControlStateMachine RcsmRun\n");
}