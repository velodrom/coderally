#pragma once
#include "CarControlStateMachineIf.hpp"
#include "CarController.hpp"

class CarControlStateMachine : public CarControlStateMachineIf, public StateMachine
{
public:
    CarControlStateMachine(CarController &carCntrlIn);

    void DriveCar(const CarControlData* carCtrlData);
    void StartCar();
    void StopCar();

    enum States
    {
        ST_READY,
        ST_RUNNING,
        ST_STOPPED,
        ST_MAX_STATES
    };

private:
    CarController &carCntrl;

    // Private state machine control functions
    BOOL RsmCarOnStartGrid(const NoEventData*);
    GuardCondition<CarControlStateMachine, NoEventData, &CarControlStateMachine::RsmCarOnStartGrid> StarCarSucceeds;

    void RsmReady(const NoEventData*);
    StateAction<CarControlStateMachine, NoEventData, &CarControlStateMachine::RsmReady> Ready;

    void RsmRunning(const CarControlData*);
    StateAction<CarControlStateMachine, CarControlData, &CarControlStateMachine::RsmRunning> Running;

    void RsmStop(const NoEventData*);
    StateAction<CarControlStateMachine, NoEventData, &CarControlStateMachine::RsmStop> Stop;

    virtual const StateMapRowEx* GetStateMapEx()
    {
        static const StateMapRowEx STATE_MAP[] =
        {
            {&Ready, &StarCarSucceeds, 0, 0},
            {&Running, 0, 0, 0},
            {&Stop, 0, 0, 0}
        };
        C_ASSERT((sizeof(STATE_MAP)/sizeof(StateMapRowEx)) == ST_MAX_STATES);
        return &STATE_MAP[0];
    }
    virtual const StateMapRow*   GetStateMap() 
    {
        //static const StateMapRow STATE_MAP[] = 
        //{
        //    &Ready,
        //    &Running,
        //    &Stop
        //};
        //C_ASSERT((sizeof(STATE_MAP)/sizeof(StateMapRow)) == ST_MAX_STATES); 
        //return &STATE_MAP[0];
        return NULL;
    }
};
