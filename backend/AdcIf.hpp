#ifndef ADCIF_HPP
#define ADCIF_HPP

#include <stdint.h>
#include <vector>

using namespace std;

class AdcIf
{
public:
    virtual ~AdcIf() {};
    virtual bool     Configure() = 0;
    virtual bool     IsConfigured() = 0;
    virtual uint32_t Measure(uint32_t channel) = 0;
};


#endif
