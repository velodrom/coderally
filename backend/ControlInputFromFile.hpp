#ifndef CONTROL_INPUT_FROM_FILE_HPP
#define CONTROL_INPUT_FROM_FILE_HPP
#include <DriverAiIf.hpp>
#include <cstdio>

using namespace std;

class ControlInputFromFile : public DriverAiIf
{
public:
    ControlInputFromFile(string fileName);
    ~ControlInputFromFile();
    CarControlData ControlCar(SensorData const &sensorInput);
private:
    void OpenInputFile();

    uint32_t    currentLine;
    string      inputFile;
    FILE        *filePtr;
    CarControlData lastInput;
};

#endif
