#include <gmock/gmock.h>
#include "MsgServerIf.hpp"

class MsgServerMock : public MsgServerIf
{
public:
    MOCK_METHOD0(InitMsgServer, void());
};