#include <gmock/gmock.h>
#include "CarControlStateMachineIf.hpp"

class CarControlStateMachineMock : public CarControlStateMachineIf
{
public:
    MOCK_METHOD0(InitCarStm, void());
    //MOCK_METHOD0(CarOnStartGrid, void());
    //MOCK_METHOD0(StartCar, void());
    //MOCK_METHOD1(UpdateCarControls, void(CarControlData* carCtrlData));
    MOCK_METHOD0(Drive, void());
};