#include <gtest/gtest.h>
#include "Mcp3204.hpp"
#include "SensorDataProvider.hpp"

using namespace ::testing;

const uint32_t MIN_VALID_MEAS = 10;

class SensorDataProviderTest : public Test
{
public:
    SensorDataProviderTest()
    {
        sensorProvider = new SensorDataProvider();
    }

    ~SensorDataProviderTest()
    {
        delete sensorProvider;
    }

protected:
    SensorDataProvider *sensorProvider;
};

// Note: This unit test requires actual hardware connected
TEST_F(SensorDataProviderTest, ReadingResistorsFillsOneMeasurement)
{
    uint32_t photoArray[numPhotoResistors] = {0};
    sensorProvider->ReadPhotoResistors(photoArray);
    ASSERT_TRUE(photoArray[0] > MIN_VALID_MEAS);
}

// Note: This unit test requires actual hardware connected
TEST_F(SensorDataProviderTest, ReadingFourResistorsReturnsNonzeroMeasurements)
{
    const uint32_t numMcp3208AdcChannels = 8;
    uint32_t photoArray[numPhotoResistors] = {0};
    sensorProvider->ReadPhotoResistors(photoArray);
    for (uint32_t measIdx = 0; measIdx < numMcp3208AdcChannels; measIdx++)
    {
        ASSERT_TRUE(photoArray[measIdx] > MIN_VALID_MEAS);
    }
}

TEST_F(SensorDataProviderTest, CreateLogStringFromMeasurements)
{
    uint32_t photoArray[numPhotoResistors] = {100, 101, 102, 103, 104, 105, 106, 107};
    string logStr = sensorProvider->CreateLogString(photoArray);
    ASSERT_EQ(logStr, "PhotoArray: 0: 100 1: 101 2: 102 3: 103 4: 104 5: 105 6: 106 7: 107");
}
