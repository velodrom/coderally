#include <stdint.h>
#include <stdio.h>
#include <pigpio.h>
#include <unistd.h>
#include <PwmController.hpp>

PwmController::PwmController()
{
    if (gpioInitialise() < 0)
    {
        printf("%d: Failed to initialize GPIO\n", getpid());
    }
}

void PwmController::InitGpioPin(uint8_t gpioPin)
{
    if (gpioPin < maxGpioPin)
        gpioPins[gpioPin].isEnabled = true;
}

uint32_t PwmController::GetPwmWidth(uint8_t gpioPin)
{
    uint32_t pwnWidth = 0;
    if (GpioIsEnabled(gpioPin))
        pwnWidth = gpioPins[gpioPin].pwmWidth;
    else
        printf("%d: Invalid GPIO pin %d\n", getpid(), gpioPin);
    return pwnWidth;
}

bool PwmController::IsValidGpioPin(uint8_t gpioPin)
{
    if (gpioPin < maxGpioPin)
        return true;
    return false;
}

bool PwmController::GpioIsEnabled(uint8_t gpioPin)
{
    if (IsValidGpioPin(gpioPin))
        return gpioPins[gpioPin].isEnabled;
    return false;
}

void PwmController::SendPwmSignal(uint8_t gpioPin, uint32_t pwmWidth)
{
    if (GpioIsEnabled(gpioPin))
    {
        gpioPins[gpioPin].pwmWidth = pwmWidth;
        gpioServo(gpioPin, pwmWidth);
    }
}
