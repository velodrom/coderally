#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "RaceControlStateMachine.hpp"
#include "mocks/CarControlStateMachineMock.hpp"
#include "mocks/AiControlMock.hpp"
#include "mocks/MsgServerMock.hpp"

using namespace ::testing;

//class RaceControlStateMachineTest : public Test
//{
//public:
//    RaceControlStateMachineTest() :
//        raceStm(RaceControlStateMachine())
//    {
//    }
//
//    ~RaceControlStateMachineTest()
//    {
//    }
//
//protected:
//    RaceControlStateMachine raceStm;
//};

TEST(RaceControlStateMachineTest, InitialStateIsStopped)
{
    CarControlStateMachineMock carStmMock;
    MsgServerMock              msgServerMock;
    RaceControlStateMachine raceStm(&carStmMock, &msgServerMock);
    BYTE state = raceStm.GetCurrentState();
    EXPECT_EQ(RaceControlStateMachine::ST_STOPPED, state);
}

TEST(RaceControlStateMachineTest, FirstRaceTransitionsToReadyState)
{
    CarControlStateMachineMock carStmMock;
    MsgServerMock              msgServerMock;
    EXPECT_CALL(carStmMock, InitCarStm())
        .Times(1);
    EXPECT_CALL(msgServerMock, InitMsgServer())
        .Times(1);
    RaceControlStateMachine raceStm(&carStmMock, &msgServerMock);
    raceStm.Run();
    BYTE state = raceStm.GetCurrentState();
    EXPECT_EQ(RaceControlStateMachine::ST_READY, state);
}

TEST(RaceControlStateMachineTest, SecondRaceTransitionsToRunningState)
{
    CarControlStateMachineMock carStmMock;
    MsgServerMock              msgServerMock;
    EXPECT_CALL(carStmMock, InitCarStm())
        .Times(1);
    EXPECT_CALL(msgServerMock, InitMsgServer())
        .Times(1);
    RaceControlStateMachine raceStm(&carStmMock, &msgServerMock);
    raceStm.Run();
    raceStm.Run();
    BYTE state = raceStm.GetCurrentState();
    EXPECT_EQ(RaceControlStateMachine::ST_RUNNING, state);
}