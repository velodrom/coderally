#include <gtest/gtest.h>
#include <PwmController.hpp>

using namespace ::testing;

class PwmControllerTest : public Test
{
public:
    PwmControllerTest()
{
        pwmCntrl = new PwmController();
}
~PwmControllerTest()
{
    delete pwmCntrl;
}
protected:
    PwmController *pwmCntrl;
};

TEST_F(PwmControllerTest, InitializePwmGpioPin)
{
    int gpioPin = 18;
    pwmCntrl->InitGpioPin(gpioPin);
    ASSERT_TRUE(pwmCntrl->GpioIsEnabled(gpioPin));
}

TEST_F(PwmControllerTest, GpioPwmWidthIsInitializedToZero)
{
    int gpioPin = 18;
    pwmCntrl->InitGpioPin(gpioPin);
    uint32_t pwmWidth = pwmCntrl->GetPwmWidth(gpioPin);
    ASSERT_TRUE(pwmWidth == 0);
}

TEST_F(PwmControllerTest, SendPwmSignalWorks)
{
    int gpioPin = 18;
    pwmCntrl->InitGpioPin(gpioPin);
    uint32_t pwmWidth = 1100;
    pwmCntrl->SendPwmSignal(gpioPin, pwmWidth);
    ASSERT_EQ(pwmWidth, pwmCntrl->GetPwmWidth(gpioPin));
}
