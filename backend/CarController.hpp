#pragma once
#include "SensorDataProvider.hpp"
#include "CarEcu.hpp"
#include "ZmqServer.hpp"
#include "KillswitchIf.hpp"
// #include "boost/interprocess/ipc/message_queue.hpp"
// #include "boost/date_time/posix_time/posix_time.hpp"

// using namespace boost::interprocess;
// using namespace boost::posix_time;

class CarController
{
public:
    CarController(ZmqServer &zmqServerIn, KillswitchIf *killSwitchIn);

    void DriveLoop();
    void SendSensorDataToDriverAi(SensorData &sensorData);
    bool ReceiveControlDataFromAi(CarControlData &ctrlData);

    bool StartCar(void);
    void StopCar(void);
private:
    void SendStartSignalToSpeedController(void);

    bool               carRunning;
    uint32_t           refreshRate;
    uint32_t           loopCounter;
    SensorDataProvider *sensorProvider;
    CarEcu             *carEcu;
    ZmqServer          &zmqServer;
    KillswitchIf       *killSwitch;        
};
