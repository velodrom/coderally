#include <gtest/gtest.h>
#include "XBeeKillswitch.hpp"
#include "Timer.hpp"

using namespace ::testing;

TEST(XBeeKillswitchTest, TimerTest)
{
    Timer t1;
    sleep(1);
    ASSERT_EQ(1000, t1.Elapsed());
}

// Note that these tests are manual tests that require hardware
TEST(XBeeKillswitchTest, ReadXBeeApiCommandStartByte)
{
    XBeeKillswitch *xbeeKillswitch = new XBeeKillswitch();
    sleep(1); // Allow XBee time to send multiple packets
    KillswitchStatus switchStatus = xbeeKillswitch->GetKillswitchStatus();
    ASSERT_EQ(SWITCH_ON, switchStatus);
}
