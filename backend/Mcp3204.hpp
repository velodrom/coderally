#ifndef MCP3204_HPP
#define MCP3204_HPP
#include "AdcIf.hpp"
#include <linux/spi/spidev.h>

//extern "C"
//{

typedef struct spi_ioc_transfer SpiTransfer;
const uint32_t spiTransferLen = 3;

class Mcp3204 : public AdcIf
{
public:
    Mcp3204();
    ~Mcp3204();
    virtual bool     Configure();
    virtual bool     IsConfigured();
    virtual uint32_t Measure(uint32_t channel = 0);
    SpiTransfer*     CreateMeasurementRequest(uint32_t channel);
    void             DeleteMeasurementRequest(SpiTransfer *);
    uint32_t         ExtractMeasurement(SpiTransfer *);
private:
    uint32_t OpenDeviceFileDescriptor();
    uint32_t SetDeviceSpiMode();
    uint32_t SetDeviceSpiBitsPerWord();
    uint32_t SetDeviceSpiMaxSpeed();

    const char *deviceName;
    bool        adcConfigured;
    int         device; // Device file descriptor
    uint8_t     mode;
    uint8_t     bits;
    uint32_t    speed;
    uint16_t    delay;

};

//}

#endif
