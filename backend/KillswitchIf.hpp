#ifndef KILL_SWITCH_IF_HPP
#define KILL_SWITCH_IF_HPP
#include <stdint.h>

typedef enum KillswitchStatus
{
    SWITCH_OFF        = 0,
    SWITCH_ON         = 1,
    SWITCH_UNRELIABLE = 2  // Switch status is unknown or unreliable
} KillswitchStatus;

const uint32_t maxSwitchUpdateDelayInMs = 1000; // milliseconds

class KillswitchIf
{
public:
    virtual KillswitchStatus GetKillswitchStatus() = 0;
};

#endif