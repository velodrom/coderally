#include "KillswitchFactory.hpp"

KillswitchFactory::KillswitchFactory()
{
}

KillswitchFactory KillswitchFactory::GetFactory()
{
    static KillswitchFactory factory;
    return factory;
}

KillswitchIf* KillswitchFactory::CreateKillswitch(KillswitchType type)
{
    KillswitchIf* killswitch = NULL;
    switch (type)
    {
        case XBEE_KILLSWITCH:
            killswitch = new XBeeKillswitch();
            break;
        case ALWAYS_ON:
            // Not implemented yet. Continue to default behavior
        default:
            printf("%d: Undefined Killswitch type: %d\n", getpid(), (uint32_t) type);
    }

    return killswitch;
}