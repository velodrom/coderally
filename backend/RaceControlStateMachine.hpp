#ifndef RACE_CONTROL_STATE_MACHINE_HPP
#define RACE_CONTROL_STATE_MACHINE_HPP
#include "StateMachine.h"
#include "CarControlStateMachineIf.hpp"
#include "KillswitchFactory.hpp"

class RaceControlStateMachine : public StateMachine
{
public:
    RaceControlStateMachine(CarControlStateMachineIf *carStm);
    void StartRace();
    void Stop();
    void Run();

    enum States
    {
        ST_STOPPED,
        ST_READY,
        ST_RUNNING,
        ST_MAX_STATES
    };

private:
    CarControlStateMachineIf *carStm;
    KillswitchIf             *killswitch;

    void RcsmStop(const NoEventData*);
    StateAction<RaceControlStateMachine, NoEventData, &RaceControlStateMachine::RcsmStop> StopRace;

    BOOL RcsmInit(const NoEventData*);
    void RcsmReady(const NoEventData*);
    GuardCondition<RaceControlStateMachine, NoEventData, &RaceControlStateMachine::RcsmInit> InitRace;
    StateAction<RaceControlStateMachine, NoEventData, &RaceControlStateMachine::RcsmReady> ReadyToRace;

    void RcsmRun(const NoEventData*);
    StateAction<RaceControlStateMachine, NoEventData, &RaceControlStateMachine::RcsmRun> RunRace;

    virtual const StateMapRowEx* GetStateMapEx()
    {
        static const StateMapRowEx STATE_MAP[] =
        {
            {&StopRace,    0,         0, 0},
            {&ReadyToRace, &InitRace, 0, 0},
            {&RunRace,     0,         0, 0}
        };
        C_ASSERT((sizeof(STATE_MAP)/sizeof(StateMapRowEx)) == ST_MAX_STATES);
        return &STATE_MAP[0];
    }
    virtual const StateMapRow*   GetStateMap() {return NULL;}
};

#endif //RACE_CONTROL_STATE_MACHINE_HPP