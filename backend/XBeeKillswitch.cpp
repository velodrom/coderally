#include "XBeeKillswitch.hpp"
#include <functional> // std::bind
#include <unistd.h>

XBeeKillswitch::XBeeKillswitch() :
    xbee(NULL),
    xcon(NULL),
    numSwitchUpdates(0),
    logger(LoggerFactory::GetLogger())
{
    SetupLink(xbeeLocalAddress);
}

XBeeKillswitch::~XBeeKillswitch()
{
    xbee_conEnd(xcon);
    xbee_shutdown(xbee);
}

void XBeeKillswitch::UpdateKillswitch()
{
    xbeeApiFrameData *dataPtr = GetNewPacket();
    if (dataPtr != NULL)
    {
        switchStatus.lastUpdateTime.Reset();
        switchStatus.isSwitchOn = ConvertToSwitchStatus(dataPtr->adioValue);
        numSwitchUpdates++;
    }
}

bool XBeeKillswitch::ConvertToSwitchStatus(uint16_t adioValue)
{
    return (adioValue > 0 ? true : false);
}

xbeeApiFrameData* XBeeKillswitch::GetNewPacket()
{
    struct xbee_pkt  *pktPtr;
    xbeeApiFrameData *dataPtr = NULL;
    int remainingPkts       = 0;
    xbee_err retval         = xbee_conRx(xcon, &(pktPtr), &remainingPkts);

    if (retval== XBEE_ENONE)
        dataPtr = reinterpret_cast<xbeeApiFrameData*>(&(pktPtr->data));

    if (remainingPkts > 0)
        xbee_conPurge(xcon);

    return dataPtr;
}

KillswitchStatus XBeeKillswitch::GetKillswitchStatus()
{
    UpdateKillswitch();
    KillswitchStatus status = (switchStatus.isSwitchOn ? SWITCH_ON : SWITCH_OFF);

    if (switchStatus.lastUpdateTime.Elapsed() > maxSwitchUpdateDelayInMs)
        status = SWITCH_UNRELIABLE;

    return status;
}

bool XBeeKillswitch::SetupLink(uint64_t localXBeeAddr)
{
    xbee_err retval;
    struct xbee_conAddress address;
    struct xbee_pkt *pkt;

    retval = xbee_setup(&xbee, "xbee2", "/dev/serial0", 57600);
    if (retval != XBEE_ENONE) 
    {
        printf("%d: retval: %d (%s)\n", getpid(), retval, xbee_errorToStr(retval));
        logger->error("Failed to setup XBee link on /dev/serial0 with baudrate 57600: %d (%s)", retval, xbee_errorToStr(retval));
        return false;
    }

    memset(&address, 0, sizeof(address));
    address.addr64_enabled = 1;
    address.addr64[0] = (localXBeeAddr & 0xFF00000000000000L) >> 56; //0x00;
    address.addr64[1] = (localXBeeAddr & 0x00FF000000000000L) >> 48; //0x13;
    address.addr64[2] = (localXBeeAddr & 0x0000FF0000000000L) >> 40; //0xA2;
    address.addr64[3] = (localXBeeAddr & 0x000000FF00000000L) >> 32; //0x00;
    address.addr64[4] = (localXBeeAddr & 0x00000000FF000000L) >> 24; //0x40;
    address.addr64[5] = (localXBeeAddr & 0x0000000000FF0000L) >> 16; //0x99;
    address.addr64[6] = (localXBeeAddr & 0x000000000000FF00L) >>  8; //0x22;
    address.addr64[7] = (localXBeeAddr & 0x00000000000000FFL) >>  0; //0xE5;

    retval = xbee_conNew(xbee, &xcon, "I/O", &address);
    if (retval !=  XBEE_ENONE)
    {
        printf("%d: Failed A: %d (%s)", getpid(), retval, xbee_errorToStr(retval));
        logger->error("Failed to establish XBee connection: xbee_conNew() returned: %d (%s)", retval, xbee_errorToStr(retval));
        return false;
    }

    return true;
}