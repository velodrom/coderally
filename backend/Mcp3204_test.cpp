#include "gtest/gtest.h"
#include "AdcIf.hpp"
#include "Mcp3204.hpp"

TEST(Mcp3204Test, ClassCreates)
{
    Mcp3204 *adc = new Mcp3204();
    ASSERT_TRUE(adc != 0);
    delete adc;
}

TEST(Mcp3204Test, ConfigureAndOpenDevice)
{
    // Real hardware required for this unit test
    Mcp3204 *adc = new Mcp3204();
    ASSERT_TRUE(adc->Configure());
    delete adc;
}

TEST(Mcp3204Test, CreateMeasRequest)
{
    Mcp3204 *adc                = new Mcp3204();
    uint32_t channel            = 0;
    struct spi_ioc_transfer *msg = adc->CreateMeasurementRequest(channel);

    uint16_t *txBuf16   = reinterpret_cast<uint16_t*>(msg[0].tx_buf);
    uint16_t byteMask3B = 0xFFFF;
    ASSERT_EQ(0x8001, (*txBuf16) & byteMask3B);

    adc->DeleteMeasurementRequest(msg);
    delete adc;
}

TEST(Mcp3204Test, ExtractMeasurement)
{
    Mcp3204 *adc = new Mcp3204();
    uint8_t txBuf[] = {0x06, 0x00, 0x00};
    uint8_t rxBuf[] = {0x06, 0x04, 0x56};

    #pragma GCC diagnostic ignored "-Wmissing-field-initializers"
    SpiTransfer txMsg =
    {
        tx_buf   : (__u64)txBuf,
        rx_buf   : (__u64)rxBuf
    };

    uint32_t measurement = adc->ExtractMeasurement(&txMsg);
    ASSERT_EQ(0x000456, measurement);
    delete adc;
}

TEST(Mcp3204Test, RequestMeasurement)
{
    // Real hardware required for this unit test
    Mcp3204  *adc = new Mcp3204();
    adc->Configure();

    uint32_t adcMeas;
    adcMeas = adc->Measure();

    ASSERT_TRUE(adcMeas > 0);
    delete adc;
}
