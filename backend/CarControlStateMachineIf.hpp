#ifndef CAR_CONTROL_STATE_MACHINE_IF_HPP
#define CAR_CONTROL_STATE_MACHINE_IF_HPP
#include "StateMachine.h"
#include "IOStructs.hpp"

class CarControlStateMachineIf
{
public:
    virtual void StartCar() = 0;
    virtual void DriveCar(const CarControlData* carCtrlData) = 0;
    virtual void StopCar() = 0;
};

#endif //CAR_CONTROL_STATE_MACHINE_IF_HPP