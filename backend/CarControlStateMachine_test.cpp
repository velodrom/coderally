#include <gtest/gtest.h>
#include "CarControlStateMachine.hpp"
#include "CarController.hpp"
#include "ZmqClient.hpp"

using namespace ::testing;

class CarControlStateMachineTest : public Test
{
public:
    CarControlStateMachineTest()
    {
        const std::string address = "tcp://127.0.0.1:5555";
        zmqServer = new ZmqServer(address, ZMQ_PAIR);
        zmqClient = new ZmqClient();
        zmqClient->Connect(address, ZMQ_PAIR);
        carCntrl  = new CarController(*zmqServer);
        carStm    = new CarControlStateMachine(*carCntrl);
    }

    ~CarControlStateMachineTest()
    {
        delete zmqClient;
        delete zmqServer;
        delete carCntrl;
        delete carStm;
    }

protected:
    CarControlStateMachine *carStm;
    CarController          *carCntrl;
    ZmqServer              *zmqServer;
    ZmqClient              *zmqClient;
};

TEST_F(CarControlStateMachineTest, InitialStateIsStopped)
{
    BYTE state = carStm->GetCurrentState();
    EXPECT_EQ(CarControlStateMachine::ST_STOPPED, state);
}

TEST_F(CarControlStateMachineTest, CarCanStartFromStoppedWhenKillswitchInactive)
{
    carStm->StartCar();

    BYTE state = carStm->GetCurrentState();
    EXPECT_EQ(CarControlStateMachine::ST_READY, state);
}

// TEST_F(CarControlStateMachineTest, CarDoesNotStartWhenKillswitchActive)
// {
//     carStm->Start();

//     BYTE state = carStm->GetCurrentState();
//     EXPECT_EQ(CarControlStateMachine::ST_STOPPED, state);
// }

TEST_F(CarControlStateMachineTest, CarGetsRunningWhenStarted)
{
    carStm->StartCar();
    CarControlData* ctrlData = new CarControlData();
    carStm->DriveCar(ctrlData);

    BYTE state = carStm->GetCurrentState();
    EXPECT_EQ(CarControlStateMachine::ST_RUNNING, state);
}

TEST_F(CarControlStateMachineTest, CarCannotBeDrivenFromStopped)
{
    CarControlData* ctrlData = new CarControlData();
    carStm->DriveCar(ctrlData);

    BYTE state = carStm->GetCurrentState();
    EXPECT_EQ(CarControlStateMachine::ST_STOPPED, state);
}

TEST_F(CarControlStateMachineTest, UpdatingCarControlsDoesNotChangeCarState)
{
    carStm->StartCar();
    CarControlData* ctrlData = new CarControlData();
    ctrlData->throttle = 1100;
    ctrlData->steer    = 1500;
    carStm->DriveCar(ctrlData);

    BYTE state = carStm->GetCurrentState();
    EXPECT_EQ(CarControlStateMachine::ST_RUNNING, state);
}