#include "CarControlStateMachine.hpp"
#include <stdio.h>

CarControlStateMachine::CarControlStateMachine(CarController &carCntrlIn) :
    StateMachine(ST_MAX_STATES, ST_STOPPED),
    carCntrl(carCntrlIn)
{
}

void CarControlStateMachine::DriveCar(const CarControlData* carCtrlData)
{
    static const BYTE TRANSITIONS[] = 
    {                   // Current state
        ST_RUNNING,     // ST_READY
        ST_RUNNING,     // ST_RUNNING
        EVENT_IGNORED,  // ST_STOPPED
    };
    ExternalEvent(TRANSITIONS[GetCurrentState()], carCtrlData); 
    C_ASSERT((sizeof(TRANSITIONS)/sizeof(BYTE)) == ST_MAX_STATES);
}

void CarControlStateMachine::StartCar()
{
    static const BYTE TRANSITIONS[] = 
    {                  // Current state
        EVENT_IGNORED, // ST_READY
        EVENT_IGNORED, // ST_RUNNING
        ST_READY,      // ST_STOPPED
    };
    CarControlData *ctrlData = new CarControlData();
    ctrlData->steer    = idleServoPwm;
    ctrlData->throttle = idleMotorPwm;
    ExternalEvent(TRANSITIONS[GetCurrentState()], ctrlData);
    C_ASSERT((sizeof(TRANSITIONS)/sizeof(BYTE)) == ST_MAX_STATES);
}

void CarControlStateMachine::StopCar()
{
    static const BYTE TRANSITIONS[] = 
    {                   // Current state
        ST_STOPPED,     // ST_READY
        ST_STOPPED,     // ST_RUNNING
        EVENT_IGNORED,  // ST_STOPPED
    };
    ExternalEvent(TRANSITIONS[GetCurrentState()], NULL); 
    C_ASSERT((sizeof(TRANSITIONS)/sizeof(BYTE)) == ST_MAX_STATES);
}


BOOL CarControlStateMachine::RsmCarOnStartGrid(const NoEventData*)
{
    printf("CarControlStateMachine Checking if car is on starting grid and killswitch is inactive\n");
    return 1;
}

void CarControlStateMachine::RsmReady(const NoEventData*)
{
    printf("CarControlStateMachine Ready\n");
}

void CarControlStateMachine::RsmRunning(const CarControlData* carCtrlData)
{
    printf("CarControlStateMachine Running\n");
    (void) carCtrlData;
}

void CarControlStateMachine::RsmStop(const NoEventData*)
{
    printf("CarControlStateMachine Stop\n");
}
