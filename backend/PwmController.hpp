#ifndef PWM_CONTROLLER_HPP
#define PWM_CONTROLLER_HPP
#include <stdio.h>
#include <stdint.h>

const uint32_t maxGpioPin = 31;

typedef struct GpioPin
{
    GpioPin() :
        isEnabled(false),
        pwmWidth(0) {};
    bool     isEnabled;
    uint32_t pwmWidth;
} GpioPin;

class PwmController
{
public:
    PwmController();

    void     InitGpioPin(uint8_t gpioPin);
    uint32_t GetPwmWidth(uint8_t gpioPin);
    bool     IsValidGpioPin(uint8_t gpioPin);
    bool     GpioIsEnabled(uint8_t gpioPin);
    void     SendPwmSignal(uint8_t gpioPin, uint32_t pwmWidth);

private:
    GpioPin gpioPins[maxGpioPin];
};

#endif //PWM_CONTROLLER_HPP
