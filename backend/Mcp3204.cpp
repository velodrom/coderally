#include "Mcp3204.hpp"
#include <cstdlib>
#include <cstring>     // For membset
#include <fcntl.h>     // For O_RDWR
#include <unistd.h>    // For open() and close()
#include <sys/ioctl.h> // For SPI_IOC_*
#include <stdio.h>

Mcp3204::Mcp3204() :
    deviceName("/dev/spidev0.0"),
    adcConfigured(false),
    device(-1),
    mode(0),
    bits(8),
    speed(500000),
    delay(0) {}

Mcp3204::~Mcp3204()
{
    if (device != -1)
        close(device);
}

bool Mcp3204::Configure()
{
    uint32_t configureOk = 0x0;

    configureOk |= OpenDeviceFileDescriptor(); // Bit  0b0000001
    configureOk |= SetDeviceSpiMode();         // Bits 0b0000110
    configureOk |= SetDeviceSpiBitsPerWord();  // Bits 0b0011000
    configureOk |= SetDeviceSpiMaxSpeed();     // Bits 0b1100000

    if (configureOk == 0x0)
        adcConfigured = true;
    else
        printf("configure error bitmask: %d\n", configureOk);
    return adcConfigured;
}

bool Mcp3204::IsConfigured()
{
    return adcConfigured;
}


uint32_t Mcp3204::Measure(uint32_t channel)
{
    uint32_t adcMeas = 0;
    SpiTransfer *measReq = CreateMeasurementRequest(channel);
    int ret = ioctl(device, SPI_IOC_MESSAGE(spiTransferLen), measReq);
    if (ret < 1)
        printf("Could not send meas request for %s\n", deviceName);

    adcMeas = ExtractMeasurement(measReq);
    DeleteMeasurementRequest(measReq);
    return adcMeas;
}

SpiTransfer* Mcp3204::CreateMeasurementRequest(uint32_t channel)
{
    // S: differential/signle ended operation
    // D2-0: Channel selection bits
    uint8_t *txBuf = (uint8_t*)calloc(spiTransferLen, sizeof(uint8_t));
    uint8_t *rxBuf = (uint8_t*)calloc(spiTransferLen, sizeof(uint8_t));
    //txBuf[0] = 0x06 | ((channel && 0x4) >> 2); //(0x06 << 16) | // 0b  0, 0, 0, 0, 0, 1, S,D2
    //txBuf[1] = (channel && 0x3) << 6;   //(0x00 <<  8) | // 0b D1,D0, X, X, X, X, X, X
    //txBuf[2] = 0x00; //(0x00);                           // 0b  X, X, X, X, X, X, X, X
    txBuf[0] = 1;                              // Start bit
    txBuf[1] = 0x80 | ((channel & 0x7) << 4);  // 0b S,D2,D1,D0, X, X, X, X
    txBuf[2] = 0;                              // 0b X, X, X, X, X, X, X, X

    // Create one SpiTransfer for each byte
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
    SpiTransfer *txMsg = (SpiTransfer*)malloc(sizeof(SpiTransfer)*spiTransferLen);
    for (uint32_t byteIdx = 0; byteIdx < spiTransferLen; byteIdx++)
    {
        txMsg[byteIdx].tx_buf        = (__u64)(txBuf+byteIdx);
        txMsg[byteIdx].rx_buf        = (__u64)(rxBuf+byteIdx);
        txMsg[byteIdx].len           = sizeof(*(txBuf));
        txMsg[byteIdx].speed_hz      = speed;
        txMsg[byteIdx].delay_usecs   = delay;
        txMsg[byteIdx].bits_per_word = bits;
        txMsg[byteIdx].cs_change     = 0;
        txMsg[byteIdx].pad           = 0;
        txMsg[byteIdx].tx_nbits      = 0;
        txMsg[byteIdx].rx_nbits      = 0;
    }
    
    return txMsg;
}

void Mcp3204::DeleteMeasurementRequest(SpiTransfer *measReq)
{
    uint8_t *txBuf = (uint8_t*)measReq->tx_buf;
    uint8_t *rxBuf = (uint8_t*)measReq->rx_buf;

    free(txBuf);
    free(rxBuf);
    free(measReq);
}

uint32_t Mcp3204::ExtractMeasurement(SpiTransfer *measReq)
{
    uint8_t *rxBuf = (uint8_t*)measReq[0].rx_buf;
    //printf("rxBuf: 0x%02X%02X%02X\n", rxBuf[0], rxBuf[1], rxBuf[2]);
    // Mcp3204 measurement is filled in a rx_buf of size 3 bytes.
    // The first 10bits is a copy of transmit signal, and rest 14 bits
    // consists of the measurement
    uint32_t measurement = ((rxBuf[1] & 0xF) << 8) | rxBuf[2];
    return measurement;
}

uint32_t Mcp3204::OpenDeviceFileDescriptor()
{
    device = open(deviceName, O_RDWR);
    if (device < 0)
        return 0x1;
    return 0x0;
}

uint32_t Mcp3204::SetDeviceSpiMode()
{
    uint32_t spiOk = 0x0;
    int ret;

    ret = ioctl(device, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
    {
        spiOk |= 0x1 << 1;
        printf("can't set spi mode\n");
    }

    ret = ioctl(device, SPI_IOC_RD_MODE, &mode);
    if (ret == -1)
    {
        spiOk |= 0x1 << 2;
        printf("can't get spi mode\n");
    }

    return spiOk;
}

uint32_t Mcp3204::SetDeviceSpiBitsPerWord()
{
    uint32_t spiOk = 0x0;
    int ret;

    ret = ioctl(device, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1)
    {
        spiOk |= 0x1 << 3;
        printf("can't set bits per word\n");
    }

    ret = ioctl(device, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1)
    {
        spiOk |= 0x1 << 4;
        printf("can't get bits per word\n");
    }

    return spiOk;
}

uint32_t Mcp3204::SetDeviceSpiMaxSpeed()
{
    uint32_t spiOk = 0x0;
    int ret;

    ret = ioctl(device, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
    {
        spiOk |= 0x1 << 5;
        printf("can't set max speed hz\n");
    }

    ret = ioctl(device, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1)
    {
        spiOk |= 0x1 << 6;
        printf("can't get max speed hz\n");
    }

    return spiOk;
}
