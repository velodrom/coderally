#ifndef CAR_ECU_HPP
#define CAR_ECU_HPP
#include "PwmController.hpp"
#include "IOStructs.hpp"
#include "HWConfig.hpp"

class CarEcu
{
public:
    CarEcu();
    void ControlCar(CarControlData &ctrlData);
    uint32_t LimitMotorPwm(uint32_t motorPwm);
    uint32_t LimitServoPwm(uint32_t servoPwm);

private:
    PwmController *pwmCtrl;
};

#endif
