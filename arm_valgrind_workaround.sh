#!/bin/bash

# The stock version of memcmp in Raspbian uses an assembly instruction which current Valgrind simply can't handle.
# Solution for using the valgrind is remove arm-mem temporarily then use the original implementation in libc

function swap_arm_mem_to_libc()
{
sudo mv /usr/lib/arm-linux-gnueabihf/libarmmem.so /usr/lib/arm-linux-gnueabihf/libarmmem.so.orig
sudo ln -s  /lib/arm-linux-gnueabihf/libc.so.6  /usr/lib/arm-linux-gnueabihf/libarmmem.so
sudo ldconfig
}

function return_arm_mem()
{
sudo rm /usr/lib/arm-linux-gnueabihf/libarmmem.so
sudo mv /usr/lib/arm-linux-gnueabihf/libarmmem.so.orig /usr/lib/arm-linux-gnueabihf/libarmmem.so
sudo ldconfig
}

#swap_arm_mem_to_libc
return_arm_mem
