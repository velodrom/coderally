#!/bin/bash
ROOT_DIR="$(dirname "$(pwd)")"
GTEST_DIR=${ROOT_DIR}/gtest
BOOST_DIR=${ROOT_DIR}/boost_1_64_0
SPDLOG_DIR=${ROOT_DIR}/spdlog
PIGPIO_DIR=${ROOT_DIR}/PIGPIO
LIBXBEE3_DIR=${ROOT_DIR}/libxbee3
LIBZMQ_DIR=${ROOT_DIR}/libzmq
CPPZMQ_DIR=${ROOT_DIR}/cppzmq

function clone_gtest(){
	git clone https://github.com/google/googletest.git ${GTEST_DIR}
}

function get_boost(){
	wget https://sourceforge.net/projects/boost/files/boost/1.64.0/boost_1_64_0.tar.bz2 -P ${ROOT_DIR}
	cd ${ROOT_DIR}
	tar --bzip2 -xf boost_1_64_0.tar.bz2
}

function install_boost(){
	cd ${BOOST_DIR}
	./bootstrap.sh --prefix=${ROOT_DIR}
	./b2 install
}

function get_spdlog(){
	cd ${SPDLOG_DIR}
	git clone https://github.com/gabime/spdlog.git ${SPDLOG_DIR}
}

function install_pigpio(){
	wget abyz.co.uk/rpi/pigpio/pigpio.zip -P ${ROOT_DIR}
	cd ${ROOT_DIR}
	unzip pigpio.zip
	cd ${PIGPIO_DIR}
	make
	sudo make install
}

function get_libxbee(){
	git clone https://github.com/attie/libxbee3.git ${LIBXBEE3_DIR}
	cd ${LIBXBEE3_DIR}
	make configure
	make all
}

function install_libzmq(){
	#echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
	#wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
	#apt-get install libzmq3-dev
	git clone https://github.com/zeromq/libzmq.git ${LIBZMQ_DIR}
	cd ${LIBZMQ_DIR}
	mkdir build
	cd build
	cmake ..
	sudo make install
}

function install_cppzmq(){
	#echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
	#wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
	#apt-get install libzmq3-dev
	git clone https://github.com/zeromq/cppzmq.git ${CPPZMQ_DIR}
	cd ${CPPZMQ_DIR}
	mkdir build
	cd build
	cmake ..
	sudo make install
}

echo "Starting installation.. this will take a while. Go grab some coffee."
# Clone google test repo
clone_gtest
get_boost
#install_boost # No need to build boost, can be used as header implementation
get_spdlog
install_pigpio
get_libxbee
install_libzmq
install_cppzmq

echo "You still need make sure SPI device is enabled to allow communication to ADC"
echo "Ensure 'dtparam=spi=on' isn't commented out in /boot/config.txt, and reboot"
echo ""
