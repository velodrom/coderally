#include <unistd.h>
#include <stdio.h>
#include <dlfcn.h>
#include "AiController.hpp"
#include "ControlInputFromFile.hpp"
#include "DriverAiIf.hpp"
#include "IOStructs.hpp"

AiController::AiController(ControllerType ctrlType, const std::string &inputFileName)
{
    switch (ctrlType)
    {
        case ControllerType::InputFromFile:
        {
            driverAi = new ControlInputFromFile(inputFileName);
            break;
        }
        case ControllerType::UserAiLib:
        {
            printf("%d: Starting DriverAI process & loading dynamic library\n", getpid());
            void *dlHandle = dlopen("autopilot/libdriverai.so", RTLD_NOW);
            if(dlHandle == NULL){
                printf(dlerror());
                exit(-1);
            }

            DriverAiIf *(*mkr)()= (DriverAiIf*(*)())dlsym(dlHandle, "Creator");
            driverAi = (*mkr)();
            break;
        }
        default:
        {
            printf("%d: Unsupported car control type: %d", getpid(), ctrlType);
            exit(-1);
        }
    }
}

AiController::~AiController()
{
    zmqClient.Disconnect();
}

bool AiController::ConnectToServer(const std::string &address, const int &socketTypeArg)
{
   return zmqClient.Connect(address, socketTypeArg);
}

void AiController::MessageLoop()
{
    try
    {
        while(1)
        {
            SensorData sensorData   = WaitForSensorData();
            CarControlData ctrlData = ForwardSensorDataToAi(sensorData);
            SendControlDataToCar(ctrlData);
            sleep(1);
        }

    }
    catch(const std::exception& e)
    {
        fflush(stdout);
        printf("%s:%d failed with exception \"%s\"\n", __FILE__, __LINE__, e.what());
    }
}

SensorData AiController::WaitForSensorData()
{
    SensorData sensorData;
    // message_queue::size_type recvd_size;

    printf("%d: AI Waiting for sensor data\n", getpid());
    std::string recMsg        = zmqClient.Recv(5000);
    MsgContainer msgContainer = ZmqService::DeserializeMsg(recMsg);
    if (msgContainer.header.msgId == SensorDataMsg)
    {
        sensorData = msgContainer.payload.sensorData;
        printf("%d: AI Received sensor data: %s\n", getpid(), recMsg.c_str());
    }

    return sensorData;
}

CarControlData AiController::ForwardSensorDataToAi(const SensorData &sensorData)
{
    return driverAi->ControlCar(sensorData);
}

void AiController::SendControlDataToCar(const CarControlData &controlData)
{
    printf("%d: AI Sending ctrl data to car\n", getpid());
    MsgContainer msgContainer(controlData);
    std::string serializedMsg = ZmqService::SerializeMsg(msgContainer);
    zmqClient.Send(serializedMsg, zmq::send_flags::none, 5000);
}
