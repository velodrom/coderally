#include <gtest/gtest.h>
#include <sstream>
#include "IOStructs.hpp"
#include "MsgContainer.hpp"

TEST(IoDataSerializeTest, SerializeCarControlData)
{
    CarControlData ctrlDataIn{.steer = 11, .throttle = 12};
    CarControlData ctrlDataOut;
    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive outArch(ss);
        outArch(ctrlDataIn);
    }
    {
        cereal::PortableBinaryInputArchive inArch(ss);
        inArch(ctrlDataOut);
    }
    EXPECT_EQ(ctrlDataIn.steer,    ctrlDataOut.steer);
    EXPECT_EQ(ctrlDataIn.throttle, ctrlDataOut.throttle);
}

TEST(IoDataSerializeTest, SerializeSensorData)
{
    SensorData sensorDataIn{.photoResistors = {0, 1, 2, 3, 4, 5, 6, 7}};
    SensorData sensorDataOut;
    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive outArch(ss);
        outArch(sensorDataIn);
    }
    {
        cereal::PortableBinaryInputArchive inArch(ss);
        inArch(sensorDataOut);
    }
    
    for (uint32_t i = 0; i < numPhotoResistors; i++)
    {
        EXPECT_EQ(sensorDataIn.photoResistors[i], sensorDataOut.photoResistors[i]);
    }
}

TEST(IoDataSerializeTest, SerializeMsgContainerWithCarControlData)
{
    CarControlData ctrlData{.steer = 11, .throttle = 12};
    MsgContainer ctrlContainerOut(ctrlData);
    MsgContainer ctrlContainerIn;
    EXPECT_EQ(CarControlMsg,     ctrlContainerOut.header.msgId);
    EXPECT_EQ(ctrlData.steer,    ctrlContainerOut.payload.carCtrlData.steer);
    EXPECT_EQ(ctrlData.throttle, ctrlContainerOut.payload.carCtrlData.throttle);

    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive outArch(ss);
        outArch(ctrlContainerOut);
    }
    {
        cereal::PortableBinaryInputArchive inArch(ss);
        inArch(ctrlContainerIn);
    }
    EXPECT_EQ(CarControlMsg,     ctrlContainerIn.header.msgId);
    EXPECT_EQ(ctrlData.steer,    ctrlContainerIn.payload.carCtrlData.steer);
    EXPECT_EQ(ctrlData.throttle, ctrlContainerIn.payload.carCtrlData.throttle);
}

TEST(IoDataSerializeTest, SerializeMsgContainerWithSensorData)
{
    SensorData sensorData{.photoResistors = {0, 1, 2, 3, 4, 5, 6, 7}};
    MsgContainer sensorContainerIn(sensorData);
    MsgContainer sensorContainerOut;
    EXPECT_EQ(SensorDataMsg,       sensorContainerIn.header.msgId);
    for (uint32_t i = 0; i < numPhotoResistors; i++)
    {
        EXPECT_EQ(sensorData.photoResistors[i], sensorContainerIn.payload.sensorData.photoResistors[i]);
    }

    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive outArch(ss);
        outArch(sensorContainerIn);
    }
    {
        cereal::PortableBinaryInputArchive inArch(ss);
        inArch(sensorContainerOut);
    }
    EXPECT_EQ(SensorDataMsg,     sensorContainerOut.header.msgId);
    for (uint32_t i = 0; i < numPhotoResistors; i++)
    {
        EXPECT_EQ(sensorData.photoResistors[i], sensorContainerOut.payload.sensorData.photoResistors[i]);
    }
}
