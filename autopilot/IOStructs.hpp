#pragma once
#include <stdint.h>
#include <vector>
#include <cereal/archives/portable_binary.hpp>
#include "StateMachine.h"

const uint32_t numPhotoResistors = 8;

// ESC configured for forward and reverse in range 1000-2000us.
// Throttle neutral position at 1500.
const uint32_t minServoPwm  = 1000;
const uint32_t idleServoPwm = 1500;
const uint32_t maxServoPwm  = 2000;
const uint32_t minMotorPwm  = 1400;
const uint32_t idleMotorPwm = 1500;
const uint32_t maxMotorPwm  = 1600;

struct CarControlData //: public EventData
{
    template <class Archive>
    void serialize( Archive & ar )
    {
        ar(steer, throttle);
    }

    uint32_t steer;
    uint32_t throttle;
};

struct SensorData //: public EventData
{
    template <class Archive>
    void serialize( Archive & ar )
    {
        ar(photoResistors);
    }

    uint32_t photoResistors[numPhotoResistors];
};
