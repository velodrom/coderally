#pragma once
#include "IOStructs.hpp"
#include "DriverAiIf.hpp"
#include "ZmqClient.hpp"

enum ControllerType
{
    InputFromFile = 1,
    InputCmdLine  = 2,
    UserAiLib     = 3
};

class AiController
{
public:
    AiController(ControllerType ctrlType, const std::string &inputFileName = "FixedInputData.txt");
    ~AiController();

    bool           ConnectToServer(const std::string &address, const int &socketTypeArg);
    void           MessageLoop();
    SensorData     WaitForSensorData();
    CarControlData ForwardSensorDataToAi(const SensorData &sensorData);
    void           SendControlDataToCar(const CarControlData &ctrlData);

private:
    DriverAiIf  *driverAi;
    ZmqClient    zmqClient;
};

