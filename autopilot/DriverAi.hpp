#ifndef DRIVER_AI_HPP
#define DRIVER_AI_HPP
#include <DriverAiIf.hpp>

class DriverAi : public DriverAiIf
{
public:
    DriverAi();
    CarControlData ControlCar(SensorData const &sensorInput);
};

#endif
