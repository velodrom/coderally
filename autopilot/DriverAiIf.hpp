#pragma once
#include "IOStructs.hpp"

class DriverAiIf
{
public:
    virtual CarControlData ControlCar(SensorData const &sensorInput) = 0;
};
