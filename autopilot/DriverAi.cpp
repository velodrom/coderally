#include <DriverAi.hpp>
#include <stdio.h>
#include <cstdlib>

DriverAi::DriverAi()
{
};

CarControlData DriverAi::ControlCar(SensorData const &sensorInput)
{
    CarControlData ctrlOutput;
    ctrlOutput.steer    = 1500; // Center steering
    ctrlOutput.throttle = 1500; // Idle throttle
    return ctrlOutput;
};

extern "C"
{
DriverAi *Creator()
{
    return new DriverAi();
}
}
